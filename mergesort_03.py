"""Practise for code interviews."""
import numpy as np


def mergesort(unsorted):
    """Sorts a list by recursively merging ordered halves."""
    length = len(unsorted)

    newl = []

    if length > 1:
        newl_1 = mergesort(unsorted[0:length // 2])[-1::-1]
        newl_2 = mergesort(unsorted[length // 2:])[-1::-1]

        while newl_1 or newl_2:

            if newl_1 and newl_2:
                if newl_1[-1] <= newl_2[-1]:
                    newl.append(newl_1.pop())
                elif newl_1[-1] > newl_2[-1]:
                    newl.append(newl_2.pop())
            elif newl_1:
                newl.append(newl_1.pop())
            elif newl_2:
                newl.append(newl_2.pop())

    elif length == 1:
        newl = unsorted[:]

    return newl


if __name__ == "__main__":
    # pylint: disable=invalid-name
    x_unsorted = np.random.randint(1, 101, 21)
    print(type(x_unsorted))
    x_unsorted = list(x_unsorted)
    print(type(x_unsorted))
    print("initial")
    print(x_unsorted)
    x_sorted = mergesort(x_unsorted)
    print("final")
    print(x_sorted)
    print(x_unsorted)


# pylint: disable=missing-docstring
# By convention, test names are long and descriptive and we don't
# repeat ourselves by adding docstrings.
def test_positive_integers_sort():
    unsorted = list(np.random.randint(1, 101, 21))
    expected = sorted(unsorted)
    result = mergesort(unsorted)
    assert expected == result


def test_characters_sort():
    unsorted = list("richard")
    expected = list("acdhirr")
    result = mergesort(unsorted)
    assert expected == result
