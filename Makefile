.PHONY: git-hooks
git-hooks:
	pip install --user --upgrade nbstripout # https://github.com/kynan/nbstripout/issues/69
	nbstripout --is-installed || nbstripout --install && nbstripout --install --attributes .gitattributes
