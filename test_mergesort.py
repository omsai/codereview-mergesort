import numpy as np

from mergesort import mergesort


def test_positive_integers_sort():
    unsorted = list(np.random.randint(1, 101, 21))
    expected = sorted(unsorted)
    result = mergesort(unsorted)
    assert expected == result


def test_characters_sort():
    unsorted = list("richard")
    expected = list("acdhirr")
    result = mergesort(unsorted)
    assert expected == result
