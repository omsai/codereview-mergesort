import numpy as np

def mergesort(l):
    length = len(l)

    newl = []

    if length > 1:
        newl_1 = mergesort(l[0:length // 2])[-1::-1]
        newl_2 = mergesort(l[length // 2:])[-1::-1]

        while len(newl_1) != 0 or len(newl_2) != 0:

            if len(newl_1) != 0 and len(newl_2) != 0:
                if newl_1[-1] <= newl_2[-1]:
                    newl.append(newl_1.pop())
                elif newl_1[-1] > newl_2[-1]:
                    newl.append(newl_2.pop())
            elif len(newl_1) != 0:
                newl.append(newl_1.pop())
            elif len(newl_2) != 0:
                newl.append(newl_2.pop())

    elif length == 1:
        newl = l[:]

    return newl

if __name__=="__main__":
    l = [x for x in range(10)]
    l = np.random.randint(1, 101, 21)
    print(type(l))
    l = list(l)
    print(type(l))
    print("initial")
    print(l)
    l2 = mergesort(l)
    print("final")
    print(l2)
    print(l)
